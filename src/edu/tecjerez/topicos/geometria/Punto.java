package edu.tecjerez.topicos.geometria;

public class Punto {
	
	int coordenadaX;
	int coordenadaY;
	//PuntoPrueba pp = new PuntoPrueba();
	
	//metodo para calcular la distancia entre dos puntos 
	
	double obtenerDistancia(Punto punto1, Punto punto2) {
		
		int x=punto2.coordenadaX - punto1.coordenadaX;
		int y = punto2.coordenadaY - punto1.coordenadaY;
		
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		
		//return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow(a, b));
	}
	
}

/*
 * class PuntoPrueba{ //modificador de acceso DEFAULT 
 * 	double x = new Punto().obtenerDistancia(new Punto(), new Punto());
			}
 */
	
