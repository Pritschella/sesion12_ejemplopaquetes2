package edu.tecjerez.topicos.vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import edu.tecjerez.topicos.geometria.Punto;

public class VentanaInicio extends JFrame implements ActionListener{
	public VentanaInicio() {
		getContentPane().setLayout(new FlowLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 300);
		setLocationRelativeTo(this);
		
		JPanel panelPunto = new JPanel();
		JLabel lblX1 = new JLabel("Ingresa X1: ");
		JTextField cajaX1 = new JTextField();
		
		add(panelPunto);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Punto objecto1 = new Punto();
		
		//PuntoPrueba obj2 = new PuntoPrueba();
		
		/*
		 * No se puede usar la calse PuntoPrueba ya que no 
		 * es publico y ademas esta en otro paquete. 
		 */
		
		//objecto1.obtenerDistancia(new Punto(), new Punto());
		
		
	}
}
